<?php

$sql = "
CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`password` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`email` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;
";