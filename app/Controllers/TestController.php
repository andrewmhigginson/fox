<?php

/**
 * (ɔ) Online FORMAPRO - GrCOTE7 -2022.
 */

namespace App\Controllers;

use App\Tools\Gc7;
use App\Models\User;

class TestController extends Controller
{
	public function test(): string
	{
		
		$data = (new User())->getUser(1);
		// $td->name='okéè2y';
		// $data->save();

		// Gc7::aff($data, 'data');
		return $this->template->render('pages/test.twig', ['data' => $data]);
	}

}