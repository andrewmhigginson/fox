<?php

namespace App\Validators;

use App\Contracts\Validator;
use App\FlashMessage;
use Respect\Validation\Validator as v;

class User implements Validator
{
	public static function validate(array $data): bool
	{
		$usernameIsValid = self::validateUsername($data['username']);
		$emailtIsValid   = self::validateEmail($data['email']);
		$passwordIsValid = self::validatePassword($data['password']);
		// $dateIsValid    = self::validateDate($data['date']);

		if (
			!$usernameIsValid
			|| !$emailIsValid
			|| !$passwordIsValid
			// || !$dateIsValid
		) {
			return false;
		}

		return true;
	}

	protected static function addError(string $field, string $message): void
	{
		FlashMessage::getInstance()->addError($field, $message);
	}

	private static function validateUsername(mixed $username): bool
	{
		$usernameValidator = v::alnum(' ', '-')->charset('UTF-8')->length(3, 30);

		if (!$usernameValidator->validate($username)) {
			self::addError('username', 'The username isn\t right');

			return false;
		}

		return true;
	}

	private static function validateEmail(mixed $email): bool
	{
		$emailValidator = v::alnum(' ', '-', '.')->charset('UTF-8')->length(1, 255);

		if (!$emailValidator->validate($email)) {
			self::addError('email', 'The email isn\t right');

			return false;
		}

		return true;
	}

	private static function validatePassword(mixed $password): bool
	{
		$passwordValidator = v::alnum(' ', '-', '.')->charset('UTF-8')->length(1, 255);

		if (!$passwordValidator->validate($password)) {
			self::addError('password', 'Wrong password');

			return false;
		}

		return true;
	}
}