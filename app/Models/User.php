<?php

namespace App\Models;

class User extends Model
{
	protected static string $table = 'users';

	public function all(): \IdiormResultSet|array
	{
		return \ORM::forTable(self::$table)->orderByDesc('id')->findMany();
	}

	public function getUser($id)
	{
		return \ORM::forTable(self::$table)->findOne($id);
	}

	public function create($newUser)
	{
		// echo '<hr>';
		// var_dump($newUser);
		
		[$n, $e, $p] = [$_POST['username'], 2, 3];
		// echo '<hr>';

		$user            = \ORM::forTable(self::$table)->create();
		$user->username  = $newUser['username'];
		$user->email     = $newUser['email'];
		$user->passwword = password_hash($newUser['password'], PASSWORD_DEFAULT);

		var_dump($user->username);

		// return $user->save();
	}

	public function update($updatedTodo)
	{
		// echo '<hr>';
		// var_dump($updatedTodo['id']);
		// echo '<hr>';
		// exit;
		$todo          = \ORM::forTable(self::$table)->findOne($updatedTodo['id']);
		$todo->name    = $updatedTodo['name'];
		$todo->content = $updatedTodo['content'];

		// var_dump($todo->name);

		return $todo->save();
	}
}