<?php

use App\Controllers\TestController;
use App\Controllers\UserController;
use Inphinit\Teeny;

require_once './vendor/autoload.php';
require_once './vendor/inphinit/teeny/vendor/Teeny.php';

if (!session_id()) {
	session_start();
}

$app  = new Teeny();
$user = new UserController();

$app->action('GET', '/', function () use ($user) {
	return $user->index();
});

$app->action('GET', 'register', function () use ($user) {
	return $user->register();
});
$app->action('POST', 'register', function () use ($user) {
	return $user->create();
});

// die (__FILE__.' - '.__LINE__);

$app->action('GET', 't', function () {
	return (new TestController())->test();
});

return $app->exec();