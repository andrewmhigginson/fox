<?php

/**
 * (ɔ) Online FORMAPRO - GrCOTE7 -2022.
 */

const APP = [
	'name'     => 'APP NAME',
	'database' => [
		'host'     => 'localhost',
		'user'     => 'root',
		'password' => '',
		'dbname'   => 'fox',
	],
	'templating' => [
		'cache'     => false,
		'templates' => 'path/to/views',
	],
];